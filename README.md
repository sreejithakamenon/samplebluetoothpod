Tutorial video

https://gitlab.com/kumaraswin/bluetooth/-/blob/master/pod_explanation.mov


Bluetooth commands

"Forward"
"Left"
"Stop"
"Right"
"Reverse"
"RightHandShake"
"LeftHandShake"
"LoofRight"
"LoofLeft"
"NormalEyes"
"HappyEyes"
"ListeningEyes"
"SpeakingEyes"
"FallbackEyes"
"WinkRightEye"
"WinkLeftEye"

Pod command:

pod 'InventoRobotLib', :git => 'https://gitlab.com/sreejithakamenon/inventorobotlib'
