//
//  ViewController.swift
//  SampleBluetoothStoryboard
//
//  Created by Sreejith K Menon on 04/05/20.
//  Copyright © 2020 Sreejith K Menon. All rights reserved.
//

import UIKit
import InventoRobotLib
import CoreBluetooth

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
 
        BluetoothService.sharedInstance.delegate = self

        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) {_ in
            BluetoothService.sharedInstance.startScan()
        }

    }
}

extension ViewController: BluetoothServiceDelegate {
    func bluetoothDidChangeState() {

    }

    func bluetoothDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {

    }

    func bluetoothDidReceiveString(_ message: String) {

    }

    func bluetoothDidReceiveBytes(_ bytes: [UInt8]) {

    }

    func bluetoothDidReceiveData(_ data: Data) {

    }

    func bluetoothDidReadRSSI(_ rssi: NSNumber) {

    }

    func bluetoothDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?) {

        BluetoothService.sharedInstance.connectToBluetooth(peripheral)

        BluetoothService.sharedInstance.stopScan()
    }

    func bluetoothDidConnect(_ peripheral: CBPeripheral) {
        
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) {_ in
            BluetoothService.sharedInstance.sendCommandToRobot(command: "Stop")
        }
        
    }

    func bluetoothDidFailToConnect(_ peripheral: CBPeripheral, error: NSError?) {

    }

    func bluetoothIsReady(_ peripheral: CBPeripheral) {

    }
}
